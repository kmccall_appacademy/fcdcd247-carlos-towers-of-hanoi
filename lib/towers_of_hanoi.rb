# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def move(from, to)
    @towers[to] << @towers[from].pop
  end

  def valid_move?(from, to)
    !(@towers[from].empty? || @towers[from].last > (@towers[to].last || 10))
  end

  def won?
    @towers[0].length == 0 && @towers[1..2].map(&:length).include?(0)
  end

  def render
    (0..2).to_a.reverse_each do |i|
      row = ""
      3.times do |j|
        if @towers[j].length > i
          row += @towers[j][i].to_s
        else
          row += " "
        end
      end
      puts(row)
    end
  end

  def play
    loop do
      self.render
      if self.won?
        puts("Congratulations! You won!")
        break
      end
      puts("Take a disc from tower [1-3]: ")
      from_disc = gets.chomp.to_i - 1
      unless (0..2).include?(from_disc)
        puts("Invalid input!")
        next
      end
      puts("Put on tower [1-3]: ")
      to_disc = gets.chomp.to_i - 1
      unless (0..2).include?(to_disc)
        puts("Invalid input!")
        next
      end
      unless self.valid_move?(from_disc, to_disc)
        puts("Invalid move!")
        next
      end
      self.move(from_disc, to_disc)
    end
  end

end
